# 🐳 Docker Compose Templates Repository 🐳

[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Welcome to the Docker Compose Templates Repository! This repository contains a collection of Docker Compose templates to help you define and manage multi-container Docker applications with ease.

## 🔧 How to Use

1. First, clone this repository to your local machine.
git clone git@gitlab.com:vmgomez/docker-templates.git

2. Browse the `templates/` directory to find the Docker Compose templates organized by projects or services.

3. Navigate to the specific template's directory and modify the `docker-compose.yml` file according to your application's requirements.

4. Deploy your multi-container Docker application using Docker Compose.
docker-compose up -d


## 📝 Contributing

We welcome contributions from the community! If you find any issues or have improvements to suggest, please feel free to open an issue or create a pull request.

## 📜 License

This project is licensed under the [MIT License](LICENSE).

## 📧 Contact

If you have any questions or need assistance, you can send me message:

Happy Docker Composing! 🐳🚢