#!/usr/bin/python

from jinja2 import Environment, FileSystemLoader
import boto3
import os
import datetime
from botocore.config import Config
# from emails.o365 import Email


class AwsDats:
    def __init(self):
        self.profile = None
        self.ec2_instances = []
        self.key_id = None
        self.secret_access_key = None
        self.endpoint = None
        self.dst_mail = None
        self.region = None
        self.tag = None
        self.localstack = False


env = Environment(loader=FileSystemLoader('template'))
template = None
template2 = env.get_template('template2.html.j2')


def remove_all_duplicates(envs):
    for c_env in envs:
        for one_env in envs:
            if c_env.tag != one_env.tag:
                for one_env_ec2_instance in one_env.ec2_instances:
                    for c_env_ec2_instance in c_env.ec2_instances:
                        print(one_env_ec2_instance.get("id") + " vs " + c_env_ec2_instance.get("id"))


def exist_element_db(array, value):
    if len(array) == 0:
        return False

    for one_element in array:
        if one_element.get('addr') == value.get("addr"):
            return True

    return False


def exist_element(array, value):
    if len(array) == 0:
        return False

    for one_element in array:
        if one_element.get("id") == value.get("id"):
            return True

    return False


def list_rds(aws_prm, aws_dats_lista):
    session = boto3.Session(profile_name=aws_prm.profile,
                            region_name=aws_prm.region)
    rds = session.client('rds')
    db_instances = rds.describe_db_instances()
    ret_instance = []

    for instance in db_instances.get('DBInstances'):
        instance_dict = {
            "instance_id": instance['DBInstanceIdentifier'],
            "status": instance['DBInstanceStatus'],
            "addr": instance['Endpoint']['Address']
        }

        for c_env in aws_dats_lista:
            for k in instance['TagList']:
                if k is not None:
                    if k.get("Key") == "env":
                        if k.get("Value") == c_env.tag:
                            if exist_element_db(c_env.rds_instances, instance_dict) is False:
                                c_env.rds_instances.append(instance_dict)
    return 0x0


def list_ec2(aws_props, aws_dats_lista):
    ret_instances = []
    env_tag = None

    # AWS enums
    if aws_props.localstack is False:
        session = boto3.Session(
            profile_name=aws_props.profile,
            region_name=aws_props.region,
            aws_access_key_id=aws_props.key_id,
            aws_secret_access_key=aws_props.secret_access_key)

        ec2 = session.resource('ec2')
        ec2_client = session.client("ec2")

        for instance in ec2.instances.all():
            public_ip_address = "unknown"

            if instance.public_ip_address is not None:
                public_ip_address = instance.public_ip_address

            state = None
            if instance.state.get("Name") == "running":
                state = "up"
            else:
                state = "down"

            name_tag = "unknow"

            for current_tag in instance.tags:
                if current_tag.get("Key") == "Name":
                    name_tag = current_tag.get("Value")
                if current_tag.get("Key") == "env":
                    env_tag = current_tag.get("Value")

            platform = instance.platform
            os_version = "AMI unvailable"
            response = ec2_client.describe_images(ImageIds=[instance.image_id])

            for ami in response['Images']:
                platform = ami.get("PlatformDetails")
                os_version = ami.get("Name")

                if len(os_version) > 30:
                    os_version = os_version[:30]

            host_dict = {'hostname': None,
                         "id": instance.instance_id,
                         "tags": name_tag,
                         "private_ip": public_ip_address,
                         'ip_addr': instance.public_ip_address,
                         'private_ip_addr': instance.private_ip_address,
                         'status': state,
                         'dns_name': instance.public_dns_name,
                         'os': platform,
                         'os_version': os_version,
                         'html_path': None}

            # print(name_tag)
            # print(env_tag)

            if aws_props.tag == env_tag:
                if exist_element(ret_instances, host_dict) is False:
                    ret_instances.append(host_dict)
            else:
                for one_aws_cred in aws_dats_lista:
                    if one_aws_cred.tag == env_tag:
                        if exist_element(one_aws_cred.ec2_instances, host_dict) is False:
                            one_aws_cred.ec2_instances.append(host_dict)

    # localstack enum
    else:
        my_config = Config(
            region_name=aws_props.region,
            signature_version='v4',
            retries={'max_attempts': 10, 'mode': 'standard'})

        ret_instances = []

        ec2 = boto3.client("ec2",
                           endpoint_url=aws_props.endpoint,
                           config=my_config)

        reservations = ec2.describe_instances().get("Reservations")

        for reservation in reservations:
            for instance in reservation["Instances"]:
                response = ec2.describe_images(ImageIds=[instance.get("ImageId")])
                os_version = "AMI unvailable"

                for ami in response['Images']:
                    if 'Tags' in ami:
                        ami_platform = ami.get("Platform")
                        os_version = ami.get("Description")

                        if len(os_version) > 30:
                            os_version = os_version[:30]

                state = None

                if instance.get("State").get("Name") == "running":
                    state = "up"
                else:
                    state = "down"

                name_tag = "unknow"
                env_tag = None

                all_keys = instance.keys()
                if "Tags" in all_keys:
                    for current_tag in instance.get("Tags"):
                        if current_tag.get("Key") == "Name":
                            name_tag = current_tag.get("Value")
                        if current_tag.get("Key") == "env":
                            env_tag = current_tag.get("Value")

                host_dict = {
                    'id': instance.get('InstanceId'),
                    'hostname': None,
                    "tags": name_tag,
                    "private_ip": instance.get('PrivateDnsName'),
                    'ip_addr': instance.get('PrivateIpAddress'),
                    'private_ip_addr': instance.get('PrivateIpAddress'),
                    'status': state,
                    'dns_name': instance.get('PublicDnsName'),
                    'os': instance.get('Platform'),
                    'os_version': os_version,
                    'html_path': None
                }

                # print(name_tag)
                # print(env_tag)

                if aws_props.tag == env_tag:
                    if exist_element(ret_instances, host_dict) is False:
                        ret_instances.append(host_dict)
                else:
                    for one_aws_cred in aws_dats_lista:
                        if one_aws_cred.tag == env_tag:
                            if exist_element(one_aws_cred.ec2_instances, host_dict) is False:
                                one_aws_cred.ec2_instances.append(host_dict)

    return ret_instances


def create_html(output_file_name, aws_dict):
    rds_prod = []
    ec2_prod = []
    rds_non_prod = []
    ec2_non_prod = []
    rds_pre_prod = []
    ec2_pre_prod = []

    for one_aws_dict in aws_dict:
        if one_aws_dict.tag == "prod":
            rds_prod = one_aws_dict.rds_instances
            ec2_prod = one_aws_dict.ec2_instances

        if one_aws_dict.tag == "preprod":
            rds_pre_prod = one_aws_dict.rds_instances
            ec2_pre_prod = one_aws_dict.ec2_instances

        if one_aws_dict.tag == "nonprod":
            rds_non_prod = one_aws_dict.rds_instances
            ec2_non_prod = one_aws_dict.ec2_instances

    template2.stream(rds_non_prod=rds_non_prod,
                     ec2_non_prod=ec2_non_prod,
                     ec2_pre_prod=ec2_pre_prod,
                     rds_pre_prod=rds_pre_prod,
                     ec2_prod=ec2_prod,
                     rds_prod=rds_prod).dump(output_file_name)

def main():
    ec2_enable = True
    rds_enable = True
    aws_dats_lst = []

    aws_prod_creds = AwsDats()
    aws_non_prod_creds = AwsDats()
    aws_pre_prod_creds = AwsDats()

    aws_prod_creds.tag = "prod"
    aws_pre_prod_creds.tag = "preprod"
    aws_non_prod_creds.tag = "nonprod"

    aws_pre_prod_creds.ec2_instances = []
    aws_non_prod_creds.ec2_instances = []
    aws_prod_creds.ec2_instances = []

    aws_pre_prod_creds.rds_instances = []
    aws_non_prod_creds.rds_instances = []
    aws_prod_creds.rds_instances = []

    aws_prod_creds.localstack = False
    aws_non_prod_creds.localstack = False
    aws_pre_prod_creds.localstack = False

    email_dats = {
        "o365.sendmail_endpoint": None,
        "o365.grant_type": None,
        "neptune.https_proxy": None,
        "o365.scope": None,
        "o365.client_id": None,
        "o365.client_secret": None,
        "o365.token_endpoint": None
    }

    if "TEST_MODE" in os.environ:
        if "NEPTUNE_HTTPS_PROXY_TEST" in os.environ:
            email_dats["neptune.https_proxy"] = os.environ["NEPTUNE_HTTPS_PROXY_TEST"]

        if "o365_sendmail_endpoint_test" in os.environ:
            email_dats["o365.sendmail_endpoint"] = os.environ["o365_sendmail_endpoint_test"]

        if "o365_grant_type_test" in os.environ:
            email_dats["o365.grant_type"] = os.environ["o365_grant_type_test"]

        if "o365_scope_test" in os.environ:
            email_dats["o365.scope"] = os.environ["o365_scope_test"]

        if "o365_client_id_test" in os.environ:
            email_dats["o365.client_id"] = os.environ["o365_client_id_test"]

        if "o365_client_secret_test" in os.environ:
            email_dats["o365.client_secret"] = os.environ["o365_client_secret_test"]

        if "o365_token_endpoint_test" in os.environ:
            email_dats["o365.token_endpoint"] = os.environ["o365_token_endpoint_test"]

    else:
        if "NEPTUNE_HTTPS_PROXY" in os.environ:
            email_dats["neptune.https_proxy"] = os.environ["NEPTUNE_HTTPS_PROXY"]

        if "o365_sendmail_endpoint" in os.environ:
            email_dats["o365.sendmail_endpoint"] = os.environ["o365_sendmail_endpoint"]

        if "o365_grant_type" in os.environ:
            email_dats["o365.grant_type"] = os.environ["o365_grant_type"]

        if "o365_scope" in os.environ:
            email_dats["o365.scope"] = os.environ["o365_scope"]

        if "o365_client_id" in os.environ:
            email_dats["o365.client_id"] = os.environ["o365_client_id"]

        if "o365_client_secret" in os.environ:
            email_dats["o365.client_secret"] = os.environ["o365_client_secret"]

        if "o365_token_endpoint" in os.environ:
            email_dats["o365.token_endpoint"] = os.environ["o365_token_endpoint"]

    if "AWS_DST_EMAIL" in os.environ:
        aws_non_prod_creds.dst_mail = os.environ['AWS_DST_EMAIL']
        aws_prod_creds.dst_mail = aws_non_prod_creds.dst_mail
        aws_pre_prod_creds.dst_mail = aws_non_prod_creds.dst_mail

    if "AWS_PRE_PROD_REGION" in os.environ:
        aws_pre_prod_creds.region = os.environ['AWS_PRE_PROD_REGION']

    if "AWS_PROD_REGION" in os.environ:
        aws_prod_creds.region = os.environ['AWS_PROD_REGION']

    if "AWS_NON_PROD_REGION" in os.environ:
        aws_non_prod_creds.region = os.environ['AWS_NON_PROD_REGION']

    if "AWS_NON_PROD_KEY_ID" in os.environ:
        aws_non_prod_creds.key_id = os.environ['AWS_NON_PROD_KEY_ID']

    if "AWS_PRE_PROD_KEY_ID" in os.environ:
        aws_pre_prod_creds.key_id = os.environ['AWS_PRE_PROD_KEY_ID']

    if "AWS_PROD_KEY_ID" in os.environ:
        aws_prod_creds.key_id = os.environ['AWS_PROD_KEY_ID']

    if "AWS_PROD_SECRET_ACCESS_KEY" in os.environ:
        aws_prod_creds.secret_access_key = os.environ['AWS_PROD_SECRET_ACCESS_KEY']

    if "AWS_NON_PROD_SECRET_ACCESS_KEY" in os.environ:
        aws_non_prod_creds.secret_access_key = os.environ['AWS_NON_PROD_SECRET_ACCESS_KEY']

    if "AWS_PRE_PROD_SECRET_ACCESS_KEY" in os.environ:
        aws_pre_prod_creds.secret_access_key = os.environ['AWS_PRE_PROD_SECRET_ACCESS_KEY']

    if "AWS_PROD_PROFILE" in os.environ:
        aws_prod_creds.profile = os.environ['AWS_PROD_PROFILE']

    if "AWS_NON_PROD_PROFILE" in os.environ:
        aws_non_prod_creds.profile = os.environ['AWS_NON_PROD_PROFILE']

    if "AWS_PRE_PROD_PROFILE" in os.environ:
        aws_pre_prod_creds.profile = os.environ['AWS_PRE_PROD_PROFILE']

    if "AWS_PROD_ENDPOINT" in os.environ:
        aws_prod_creds.endpoint = os.environ['AWS_PROD_ENDPOINT']

    if "AWS_NON_PROD_ENDPOINT" in os.environ:
        aws_non_prod_creds.endpoint = os.environ['AWS_NON_PROD_ENDPOINT']

    if "AWS_PRE_PROD_ENDPOINT" in os.environ:
        aws_pre_prod_creds.endpoint = os.environ['AWS_PRE_PROD_ENDPOINT']

    aws_dats_lst.append(aws_non_prod_creds)
    aws_dats_lst.append(aws_prod_creds)
    aws_dats_lst.append(aws_pre_prod_creds)

    # Check o365 var list
    for email_creds in email_dats:
        if email_dats[email_creds] is None:
            print(email_creds + " is None")
            exit(-1)

    # Check aws creds
    for one_aws_cred in aws_dats_lst:
            if one_aws_cred.secret_access_key is None or one_aws_cred.region is None or one_aws_cred.key_id is None or one_aws_cred.endpoint is None or one_aws_cred.dst_mail is None:
                print("[!] On " + one_aws_cred.tag + " ERROR")
                exit(-1)

    output_file_name = "another-name.html"
    ec2_instances = []

    if ec2_enable:
        for one_aws_cred in aws_dats_lst:
            ec2_instances = list_ec2(one_aws_cred, aws_dats_lst)

            if len(ec2_instances) > 0x0:
                for one_ec2 in ec2_instances:
                    if exist_element(one_aws_cred.ec2_instances, one_ec2) is False:
                        one_aws_cred.ec2_instances.append(one_ec2)

    rds_endpoints = []

    if rds_enable:
        for one_aws_cred in aws_dats_lst:
            list_rds(one_aws_cred, aws_dats_lst)

    create_html(output_file_name, aws_dats_lst)

    with open(output_file_name, "r", encoding="utf-8") as fp:
        Email(email_dats).send(to=aws_non_prod_creds.dst_mail, subject="blablabla", body=fp.read(), cc=None)

    print("[+] Finish!")


def send_mail_ses(mail_data, aws_prm):
    my_config = Config(
        region_name=aws_prm.get("region"),
        signature_version='v4',
        retries={'max_attempts': 10, 'mode': 'standard'})

    ses = boto3.client("ses",
                        endpoint_url=aws_prm.get("endpoint"),
                       config=my_config)

    # ses.verify_email_identity(EmailAddress=mail_data.get("sender"))

    ret = ses.send_email(
        Message={
            "Body": {
                "Html": {
                    "Charset": "UTF-8",
                    "Data": mail_data.get('raw_messeage').get("Data"),
                }
            },
            "Subject": {
                "Charset": "UTF-8",
                "Data": "EC2 report",
            },
            },
        Source=mail_data.get("sender"),

        Destination={
            "ToAddresses": [
                mail_data.get("destination"),
            ],
        })

    if ret.get('ResponseMetadata').get('HTTPStatusCode') == 200:
        print("Mail sent! using SES message ID: " + ret['MessageId'])


if __name__ == "__main__":
    print(f"[INFO] {datetime.datetime.now().strftime('%b %d %Y %H:%M:%S')} Checking hosts...")
    main()
    print(f"[INFO] {datetime.datetime.now().strftime('%b %d %Y %H:%M:%S')} Finished checking hosts")