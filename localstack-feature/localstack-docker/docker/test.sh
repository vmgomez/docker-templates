#!/bin/sh

region_var="us-gov-west-1"

for i in $(seq 1 10); do
    awslocal ec2 run-instances --region $region_var  --image-id ami-1ecc1e67 --instance-type t2.micro --key-name MyKeyPair
done

all_instances=$(awslocal ec2 describe-instances --filters Name=instance-state-name,Values=running --query "Reservations[*].Instances[*].InstanceId" --output text --region $region_var)

for resource_id in $all_instances
do
    rand_string=$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-20} | head -n 1)
    awslocal ec2 create-tags --resources $resource_id --tags Key=Name,Value=$rand_string --region $region_var
done